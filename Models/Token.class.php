<?php

namespace User\Models;
use Core\Models\BaseModel;

/**
 * Tokens are expiring authentication keys that allow users
 * to call resources in a session.
 *
 * @package IndigoStorm\Auth
 */
class Token extends BaseModel{

    protected $revisionHandling = SAVE_REVISIONS_LOG;
    protected $defaultBackupActivity = DELETE_NOBACKUP;

    public function generateToken($uses = -1, $age = 900, $tokenScope = null, $userId = null){
        global $indigoStorm;

        $security = $indigoStorm->getConfig('security');
        $globalSalt = $security->getGlobalSalt();

        $base = (!is_null($userId) ? $userId : '' ) . (!is_null($tokenScope) ? $tokenScope : '' ) . $globalSalt;

        $tokenString = uniqid($base, true);

        $this->setName(hash('sha256', $tokenString));
        if(!is_null($userId)){
            $this->setMetadata('user', $userId);
        }
        $this->setMetadata('expires', time() + $age);
        $this->setMetadata('uses', $uses);
        $this->setMetadata('age', $age);

        if(!is_null($tokenScope)){
            $this->setMetadata('scope', $tokenScope);
        }

        $this->persist();
    }

    public function isValid(){
        $usesRemaining = $this->getMetadata('uses') > 0 || $this->getMetadata('uses') == -1;
        $hasntExpired = time() <= intval($this->getMetadata('expires'));

        return $usesRemaining && $hasntExpired;
    }

    public function getUser(){
        if($this->getMetadata('user') && $this->isValid()){
            $user = new User($this->getMetadata('user'), SEARCH_BY_ID);
            return $user;
        }else{
            return false;
        }
    }

    public function useToken($tokenScope = null){
        if(!is_null($this->getMetadata('scope')) && $tokenScope === $this->getMetadata('scope')) {

            if (time() <= $this->getMetadata('expires')) {
                $this->setMetadata('expires', time() + intval($this->getMetadata('age')));
                $numUses = $this->getMetadata('uses');
                if ($numUses > 0) {
                    $numUses -= 1;
                    $this->setMetadata('uses', $numUses);
                    $this->persist();
                    return true;
                } elseif ($numUses == -1) {
                    $this->persist();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        }else{
            return false;
        }
    }

    public function expireToken(){
        $this->setMetadata('uses', 0);
        $this->setMetadata('expires', time());
        $this->persist();
    }

}

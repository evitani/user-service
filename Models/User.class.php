<?php
namespace User\Models;
use Core\Models\BaseModel;

class User extends BaseModel{
    protected $allowEnumeration = true;

    public function validatePassword($password){
        $userPassword = $this->getCredentials('password');
        return password_verify($password, $userPassword);
    }

    function configure(){
        $this->addDataTable('Credentials', DB2_VARCHAR_SHORT, DB2_VARCHAR_LONG);
        $this->addDataTable('Access', DB2_VARCHAR_SHORT, DB2_INT);
    }

}

<?php

namespace Services\User;

use Core\Interfaces\BaseInterface;

class AsyncAuthInterface extends BaseInterface {

    public function getTaskToken() {
        return $this->_generateToken();
    }

    public function getCronToken() {
        return $this->_generateToken('cron');
    }

    private function _generateToken($type = 'task') {

        $sessionName = false;
        if(array_key_exists('HTTP_IS_SESSION', $_SERVER)){
            //A session key is present in the header
            $sessionName = $_SERVER['HTTP_IS_SESSION'];
        }elseif(array_key_exists('session', $_GET) && $_GET['session'] !== ''){
            //A session key is present in the query string
            $sessionName = $_GET['session'];
        }

        if ($sessionName !== false) {
            return $this->sendGet('async', array($sessionName, $type))[0];
        }

    }

}

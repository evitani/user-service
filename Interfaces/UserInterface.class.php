<?php

namespace Services\User;
use Core\Db2\Models\SearchQuery;
use Core\Interfaces\BaseInterface;

class UserInterface extends BaseInterface{

    public function getUser($username){
        return $this->sendGet('user-interface', $username);
    }

    public function createUser($username, $metadata = null, $credentials = null, $access = null){
        $result = $this->sendPost('user-interface', array('username' => $username,
                                                          'metadata' => $metadata,
                                                          'credentials' => $credentials,
                                                          'access' => $access,
        ));
        return $result;
    }

    public function saveUser($user){
        $result = $this->sendPut('user-interface', $user->id, $user);
        return $result;
    }

    public function getApiUser($userToken){
        return $this->sendGet('api-user', $userToken);
    }

    public function login($username, $password){
        $result = $this->sendPost('login', array(
            'username' => $username,
            'password' => $password
        ));
        return $result;
    }

    public function searchUsers (SearchQuery $search, array $returnFormat = []): array {
        $return = (array) $this->sendPost('interface-search', [
            'query' => $search->pack(),
            'returnFormat' => $returnFormat
        ])->results;
        return $return;
    }

    public function deleteUser (int $userId): bool {
        return $this->sendDelete('user-interface', $userId) === 'true';
    }

}

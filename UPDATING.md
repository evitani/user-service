# Important update notes
## Version older than 2020-03-29

### Why update?
If you are using a version of this service older than 2020-03-29, then you must update
to use Indigo Storm's access control features. This update also brings improvements to
general authentication handling that are recommended, and is  required for running this
service in Google App Engine when using PHP 7.

### Breaking Changes
There are two breaking changes when updating from versions prior to 2020-03-29.

#### SQL Update
To use the access control features of Indigo Storm, you must run the following SQL once
on your database:
```
CREATE TABLE `Users__Access` (
     `UserId` int(11) unsigned NOT NULL AUTO_INCREMENT,
     `dataKey` varchar(128) NOT NULL,
     `dataValue` int(11) DEFAULT NULL,
     PRIMARY KEY (`UserId`,`dataKey`),
     CONSTRAINT `Users__Access>Users` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`)
     ON DELETE CASCADE ON UPDATE CASCADE
   );
```
This can be run prior to update without causing issues.

#### SAML (SSO) Handling
The SAML handler has been entirely rewritten to remove unneeded dependencies, improve
security, and reduce effort required within front-end applications to complete auth.

Environments using SAML should include a `saml` key in their configuration, containing
`email` and `name` to identify the application in metadata.

Any existing SAML integrations will need to be re-initiated using the new setup. IdP
metadata is now stored in the database, so instances of SamlIdp will need creating for
each IdP you integrate with, and their metadata stored in ImportedMetadata. Once this is
done, you can generate the required SP metadata for each IdP by calling the endpoint:
`/user/sp-meta/<idpname>`.

Importantly, the SP metadata has changed, so this must be re-shared with IdPs for them
to continue functioning correctly.

Applications wishing to authenticate using SAML should direct the user to the endpoint:
`/user/login-saml/<idpname>` for authentication. Once authentication is complete, the
user will then be redirected to the `redirectTo` URL identified in the SamlIdp, with the
session token replacing `_TOKEN_` where this is present. There is no need for the app
to then instantiate a new token, but it may need to get user information if this is
required in the application.

### Recommended Changes
In addition, it is recommended you update applications that POST to `/user/auth` to
use the new methods available within that endpoint and `user/login`. These are not
breaking changes, as the old methods are maintained currently.

<?php

namespace User\MagicLink\Payloads\Init;

use Core\Payloads\Payload;

class RequestPayload extends Payload {

    protected string $email;
    protected string $appId;

}

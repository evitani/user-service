<?php

namespace User\MagicLink\Controllers;

use Core\Controllers\BaseController;
use Core\Db2\Models\SearchQuery;
use Core\Routing\Request;
use Core\Routing\Response;
use Services\Mailman\MailmanInterface;
use User\Helpers\LoginHelper;
use User\MagicLink\Models\MagicApp;
use User\Models\User;

class InitController extends BaseController {

    public array $payload = [
        HTTP_METHOD_POST => 'User\\MagicLink\\Payloads\\Init\\RequestPayload',
    ];

    public function handlePost(Request $request, Response $response, array $args){

        $payload = $request->getParsedBody();

        $userMatchAddress = $payload->getEmail();
        $appId = $payload->getAppId();

        $app = new MagicApp($appId);

        $search = new SearchQuery(new User());
        $search->filter($search->_and(
            ['name', '=', $userMatchAddress],
            ['Credentials.magicApp' . $app->getId(), '=', true]
        ));
        $userMatch = $search->run();

        if (count($userMatch) !== 1) {
            throw new \Exception('App authentication failed', 401);
        } else {
            $user = new User($userMatch[0]);

            $helper = new LoginHelper();
            $token = $helper->createOneTimeLoginToken($user->getName());

            $mailman = new MailmanInterface();
            $mailman->immediateEmail(
                $user->getName(),
                $app->getMetadata('emailSender'),
                $app->getMetadata('displayName') . ': Your Sign In Link',
                [
                    'appName' => $app->getMetadata('displayName'),
                    'magicLink' => str_replace('_TOKEN_', $token, $app->getMetadata('signInHandler'))
                ],
                'magicLoginEmail'
            );
        }


    }

}

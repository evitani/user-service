<?php

namespace User\Controllers;

use Core\Controllers\BaseController;
use Core\Routing\Request;
use Core\Routing\Response;
use User\Models\Token;

/**  
 * Class for handling requests to /async
 * Built by the Indigo Storm developer tool
 * @package User\Controllers
 */  
class AsyncController extends BaseController{

    /**  
     * @param $request  Request  The request object from Slim
     * @param $response Response  The Slim response object
     * @param $args     array   Array of arguments available from the request
     */  
    public function handleGet(Request $request, Response $response, array $args) {
        $sessionId = $args['sessionId'];

        $expires = true;
        if (array_key_exists('purpose', $args) && strtolower($args['purpose']) === 'cron') {
            $expires = false;
        }

        $session = new Token($sessionId);
        $user = $session->getUser();

        $asyncToken = new Token;
        // Cron tokens expire after 8 years and 1 minute since their last use, non-cron tokens after 1 use or 6 hours
        $asyncToken->generateToken($expires ? 1 : -1,
                                   $expires ? 21600 : 252288060,
                                   'USER_ASYNC',
                                   $user->getId());

        return array($asyncToken->getName());

    }

}

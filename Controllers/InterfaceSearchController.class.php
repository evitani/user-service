<?php

namespace User\Controllers;

use Core\Controllers\BaseController;

use Core\Db2\Models\SearchQuery;
use Core\Routing\Request;

use Core\Routing\Response;
use User\Models\User;
use User\Payloads\InterfaceSearch\ResponsePayload;

/**  
 * Class for handling requests to /interface-search
 * Built by the Indigo Storm developer tool
 * @package User\Controllers
 */  
class InterfaceSearchController extends BaseController{

    public array $payload = [
        HTTP_METHOD_POST => 'User\\Payloads\\InterfaceSearch\\RequestPayload',
    ];

    /**  
     * @param $request  Request  The request object
     * @param $response Response  The response object
     * @param $args     array   Array deprecated, use $request->getArgs()
     */  
    public function handlePost(Request $request, Response $response, array $args){
        $payload = $request->getParsedBody();

        $search = new SearchQuery(new User());
        $search->unpack($payload->getQuery());
        $results = $search->run();

        if ($payload->getReturnFormat() == []) {
            return new ResponsePayload(['results' => $results]);
        } else {
            $outputResults = [];
            foreach ($results as $result) {
                $outputResults[] = $this->_getReturnData($payload->getReturnFormat(), $result);
            }
            return new ResponsePayload(['results' => $outputResults]);
        }

    }

    private function _getReturnData(array $format, string $username) : array {
        $user = new User($username);
        $return = [];

        foreach ($format as $key => $val) {
            if (is_int($key)) {
                $return[$val] = $this->__collectReturnData($val, $user);
            } else {
                $return[$key] = $this->__collectReturnData($val, $user, $key);
            }
        }
        return $return;
    }

    private function __collectReturnData($item, $user, $parent = null) {
        if (is_array($item) && !is_null($parent)) {
            $return = [];
            foreach ($item as $itm) {
                $return[$itm] = $this->__collectReturnData($itm, $user, $parent);
            }
        } elseif (is_string($item) && !is_null($parent)) {
            $getter = 'get' . strtoupper(substr($parent, 0, 1)) . substr($parent, 1);
            $return = $user->$getter($item);
        } elseif (is_string($item)) {
               $getter = 'get' . strtoupper(substr($item, 0, 1)) . substr($item, 1);
            $return = $user->$getter();
        } else {
            throw new \Exception('Invalid return format', 500);
        }
        return $return;
    }

}

<?php

namespace User\Controllers;

use Core\Controllers\BaseController;
use Core\Routing\Request;
use Core\Routing\Response;
use User\Models\Token;

class ApiUserController extends BaseController{

    public function handleGet(Request $request, Response $response, array $args){
        $token = new Token($args['tokenName']);
        $user = $token->getUser();

        if($token->getMetadata('scope') === 'USER_ASYNC') {
            $token->useToken('USER_ASYNC');
        }

        if($user !== false){
            return array('user' => $user->getId(), 'access' => $user->getAccess());
        }else{
            return array('_nouser' => true);
        }
    }

}

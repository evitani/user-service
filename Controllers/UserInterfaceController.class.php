<?php

namespace User\Controllers;

use Core\Controllers\BaseController;
use Core\Routing\Request;
use Core\Routing\Response;
use User\Models\User;

class UserInterfaceController extends BaseController{

    public function handleGet(Request $request, Response $response, array $args){
        $args = $request->getArgs();

        if(isset($args['userid']) && is_numeric($args['userid'])){
            $user = new User($args['userid'], SEARCH_BY_ID);
            $user->getAll();
            return $user;
        }elseif(isset($args['userid'])) {
            $user = new User($args['userid']);
            $user->getAll();
            return $user;
        }else{
            throw new \Exception('Resource not found User', 404);
        }

    }

    public function handlePost(Request $request, Response $response, array $args){
        $payload = $request->getParsedBody();

        if(isset($args['userId'])) {

            if(!array_key_exists('userId', $args)){
                throw new \Exception('No ID provided in PUT', 500);
            }

            $user = new User($args['userId'], SEARCH_BY_ID);
            $user->getAll();

            if($payload['name']){
                $user->setName($payload['name']);
            }

            if($payload['Metadata'] && $payload['Metadata']['data']){
                $user->setMetadata($payload['Metadata']['data']);
            }

            if($payload['Credentials'] && $payload['Credentials']['data']){
                $user->setCredentials($payload['Credentials']['data']);
            }

            $user->persist();
            return $user;

        }else{

            if (isset($payload['username'])) {

                if (!isset($payload['credentials']) || !is_array($payload['credentials'])) {
                    $payload['credentials'] = array();
                }

                if (!isset($payload['metadata']) || !is_array($payload['metadata'])) {
                    $payload['metadata'] = array();
                }

                if (!isset($payload['access']) || !is_array($payload['access'])) {
                    $payload['access'] = array();
                }

                $userCreated = false;

                try {
                    $user = new User($payload['username']);
                } catch (\Exception $e) {
                    $user = new User();
                    $user->setName($payload['username']);
                    $user->setMetadata($payload['metadata']);
                    $user->setCredentials($payload['credentials']);
                    foreach ($payload['credentials'] as $credentialId => $credentialContent) {
                        if ($credentialId === 'password') {
                            $user->setCredentials('password', password_hash($credentialContent, PASSWORD_DEFAULT));
                        }
                    }
                    $user->setAccess($payload['access']);
                    $user->persist();
                    $userCreated = true;
                }

                if ($userCreated) {
                    return $user;
                } else {
                    throw new \Exception('User creation failed (non-unique)', 500);
                }
            } else {
                throw new \Exception('Cannot create user without username', 500);
            }
        }

    }

    public function handlePut(Request $request, Response $response, array $args){

        $payload = $request->getParsedBody();

        if(!array_key_exists('userId', $args)){
            throw new \Exception('No ID provided in PUT', 500);
        }

        $user = new User($args['userId'], SEARCH_BY_ID);
        $user->getAll();

        if($payload['name']){
            $user->setName($payload['name']);
        }

        if($payload['Metadata'] && $payload['Metadata']['data']){
            $user->setMetadata($payload['Metadata']['data']);
        }

        if($payload['Credentials'] && $payload['Credentials']['data']){
            $user->setCredentials($payload['Credentials']['data']);
        }

        if($payload['Access'] && $payload['Access']['data']){
            $user->setAccess($payload['Access']['data']);
        }

        $user->persist();
        return $user;
    }

    public function handleDelete(Request $request, Response $response, array $args): bool{
        $args = $request->getArgs();
        $user = new User(intval($args['userId']), SEARCH_BY_ID);
        $user->delete();
        return true;
    }

}

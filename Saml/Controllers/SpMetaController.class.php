<?php

namespace User\Saml\Controllers;

use Core\Controllers\BaseController;
use Core\Payloads\File;
use Core\Payloads\Payload;
use Core\Routing\Request;
use Core\Routing\Response;
use User\Saml\Helpers\ServiceProviderHelper;

/**  
 * Class for handling requests to /sp-meta
 * Built by the Indigo Storm developer tool
 * @package User\Controllers
 */  
class SpMetaController extends BaseController{

    /**  
     * @param $request  Request  The request object from Slim
     * @param $response Response  The Slim response object
     * @param $args     array   Array of arguments available from the request
     */  
    public function handleGet(Request $request, Response $response, array $args){
        $helper = new ServiceProviderHelper($args['idp']);

        $metadata = $helper->getMetadata();
        $validUntil = date("Y-m-d\T23:59:59\Z", time() + (86400 * 365));

        $xml = "<?xml version=\"1.0\"?>
<md:EntityDescriptor xmlns:md=\"urn:oasis:names:tc:SAML:2.0:metadata\"
                     validUntil=\"$validUntil\"
                     cacheDuration=\"PT604800S\"
                     entityID=\"{$metadata['entityId']}\">
    <md:SPSSODescriptor AuthnRequestsSigned=\"false\" WantAssertionsSigned=\"false\" protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\">
        <md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified</md:NameIDFormat>
        <md:AssertionConsumerService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\"
                                     Location=\"{$metadata['AssertionConsumerService'][0]['Location']}\"
                                     index=\"0\" />
        
    </md:SPSSODescriptor>
    <md:ContactPerson contactType=\"{$metadata['contacts'][0]['contactType']}\">
        <md:GivenName>{$metadata['contacts'][0]['givenName']}</md:GivenName>
        <md:EmailAddress>{$metadata['contacts'][0]['emailAddress']}</md:EmailAddress>
    </md:ContactPerson>
</md:EntityDescriptor>";

        $response = $response->withFile(
            new File(['content' => $xml, 'mime' => 'text/xml'])
        );
    }

}

<?php

namespace User\Saml\Controllers;

use Core\Controllers\BaseController;
use Core\Routing\Request;
use SAML2\AuthnRequest;
use SAML2\Compat\ContainerSingleton;
use SAML2\Configuration\Destination;
use SAML2\Configuration\IdentityProvider;
use SAML2\Configuration\ServiceProvider;
use SAML2\HTTPRedirect;
use SAML2\Response;
use User\Helpers\LoginHelper;
use User\Saml\Models\Logger;
use User\Saml\Models\SamlContainer;
use User\Saml\Helpers\ServiceProviderHelper;
use User\Saml\Models\SamlIdp;

/**
 * Class for handling requests to /login-saml
 * Built by the Indigo Storm developer tool
 * @package User\Controllers
 */
class LoginController extends BaseController{

    private $idp;
    private $sp;
    private $container;

    private function setup($idpName){
        $this->idp = new SamlIdp($idpName);
        $this->sp = new ServiceProviderHelper($this->idp->getName());

        $this->container = new SamlContainer($this->idp->getName());
        ContainerSingleton::setContainer($this->container);
    }

    public function handleGet(Request $request, \Core\Routing\Response $response, array $args){

        $this->setup($args['idp']);

        $samlRequest = new AuthnRequest();
        $samlRequest->setId($this->container->generateId());
        $samlRequest->setIssuer($this->sp->getEntityId());
        $samlRequest->setDestination($this->idp->getSettings('url'));

        $binding = new HTTPRedirect();
        $binding->send($samlRequest);

    }

    public function handlePost(Request $request, \Core\Routing\Response $response, array $args){

        $this->setup($args['idp']);
        $payload = $request->getParsedBody();

        $inboundResponse = base64_decode($payload['SAMLResponse']);
        $dom = new \DOMDocument();
        $dom->loadXML($inboundResponse);
        $responseElement = $dom->documentElement;
        $samlResponse = new Response($responseElement);

        $processor = new Response\Processor( new Logger());

        $idp = new IdentityProvider($this->idp->getImportedMetadata());

        $sp = new ServiceProvider($this->sp->getMetadata());

        $dest = new Destination($this->sp->getEntityId());

        $formattedResponse = $processor->process($sp, $idp, $dest, $samlResponse);

        if(count($formattedResponse) === 1){
            $formattedResponse = $formattedResponse[0];
        }else{
            throw new \Exception("Response count mismatch", 500);
        }

        $nameId = $formattedResponse->getNameId()->value;
        $attributes = $this->prepareAttributes($formattedResponse->getAttributes(), $nameId);

        $loginHelper = new LoginHelper();

        $username = $loginHelper->findByCredential($args['idp'], $nameId);

        if($username === false){
            if($this->idp->getSettings('autoprovision')){
                $username = $loginHelper->provisionUser($attributes,
                                                        $this->idp->getSettings('attributeMap'),
                                                        array($this->idp->getName() => $nameId),
                                                        $this->idp->getSettings('defaultAccess')
                );
            }else{
                throw new \Exception("User not found", 404);
            }
        }

        $otlToken = $loginHelper->createOneTimeLoginToken($username);

        $url = $this->idp->getSettings('redirectTo');
        $url = str_replace('_TOKEN_', $otlToken, $url);

        header("location: " . $url);
        exit;
    }

    private function prepareAttributes($attributes, $nameId){
        $formatted = array();
        foreach($attributes as $key => $value){
            if((is_array($value) && count($value) > 1) || !is_array($value)){
                $formatted[$key] = $value;
            }elseif(is_array($value) && count($value) === 1){
                $formatted[$key] = $value[0];
            }
        }
        $formatted['nameid'] = $nameId;

        return $formatted;
    }

}

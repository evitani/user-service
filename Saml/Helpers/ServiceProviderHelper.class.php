<?php

namespace User\Saml\Helpers;

class ServiceProviderHelper{

    private $idp;

    public function __construct($idpName){
        $this->idp = $idpName;
    }

    public function getMetadata(){
        return array(
            'entityId' => $this->getEntityId(),
            'AssertionConsumerService' => $this->getACS(),
            'contacts' => $this->getContact(),
        );
    }

    public function getEntityId(){

        $url = 'https://' . $_SERVER['HTTP_HOST'];

        if (strpos($url, 'user-') === false) {
            $url .= '/user';
        }

        return $url . '/login-saml/' . $this->idp;

    }

    private function getContact(){
        global $indigoStorm;

        try{
            $contactInfo = $indigoStorm->getConfig('saml');
        }catch(\Exception $e){
            $contactInfo = null;
        }

        $return = array('contactType' => 'technical');

        if(!is_null($contactInfo)){

            if(!is_null($contactInfo->getName())){
                $return['givenName'] = $contactInfo->getName();
            }else{
                $return['givenName'] = "An Indigo Storm Application";
            }

            if(!is_null($contactInfo->getEmail())){
                $return['emailAddress'] = $contactInfo->getEmail();
            }else{
                $return['emailAddress'] = $this->getDefaultEmail();
            }

        }else{
            $return['givenName'] = "An Indigo Storm Application";
            $return['emailAddress'] = $this->getDefaultEmail();
        }

        return array($return);

    }

    private function getDefaultEmail(){
        global $indigoStorm;

        $url = $indigoStorm->getHost('user');

        if(substr($url, 0, 3) === 'api'){
            $components = explode('.', $url);
            array_shift($components);
            $url = implode(".", $components);
        }

        return "saml@" . $url;

    }

    private function getACS(){
        $url = $this->getEntityId();

        return array(
            array(
                'index' => 0,
                'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                'Location' => $url,
            ),
            array(
                'index' => 1,
                'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
                'Location' => $url,
            ),
        );
    }

}

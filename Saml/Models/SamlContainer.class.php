<?php

namespace User\Saml\Models;

use SAML2\Compat\AbstractContainer;

class SamlContainer extends AbstractContainer{

    private $idpName;

    public function __construct($idpName = null){
        $this->idpName = $idpName;
    }

    public function generateId(){
        global $indigoStorm;
        $security = $indigoStorm->getConfig('security');
        $prefix = $this->idpName . $security->getGlobalSalt();
        return uniqid($prefix, true);
    }

    public function debugMessage($message, $type){
        islog(LOG_INFO, strval($type) . " : " . strval($message));
    }

    public function redirect($url, $data = []){
        $encoded = array();
        foreach($data as $datum){
            $encoded[] = $datum;
        }
        $fragment = implode('/', $encoded);
        $dest = $url . $fragment;
        header("location: " . $dest);
    }

    public function postRedirect($url, $data = []){
        $params = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if ($fp) {
            echo @stream_get_contents($fp);
            die();
        }
    }

    public function getLogger(){
        return new Logger();
    }

}

<?php

namespace User\Saml\Models;

use Psr\Log\LoggerInterface;

class Logger implements LoggerInterface{

    public function alert($message, array $context = array()){
        islog(LOG_ALERT, $message);
    }

    public function critical($message, array $context = array()){
        islog(LOG_CRIT, $message);
    }

    public function debug($message, array $context = array()){
        islog(LOG_DEBUG, $message);
    }

    public function emergency($message, array $context = array()){
        islog(LOG_EMERG, $message);
    }

    public function error($message, array $context = array()){
        islog(LOG_ERR, $message);
    }

    public function info($message, array $context = array()){
        islog(LOG_INFO, $message);
    }

    public function log($level, $message, array $context = array()){
        islog(LOG_INFO, $message);
    }

    public function notice($message, array $context = array()){
        islog(LOG_NOTICE, $message);
    }

    public function warning($message, array $context = array()){
        islog(LOG_WARNING, $message);
    }

}

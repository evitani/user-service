<?php
namespace User\Payloads\InterfaceSearch;

use Core\Payloads\Payload;

class ResponsePayload extends Payload {

    protected array $results;

}

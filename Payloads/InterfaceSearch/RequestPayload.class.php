<?php

namespace User\Payloads\InterfaceSearch;

use Core\Payloads\Payload;

class RequestPayload extends Payload {

    protected array $query;
    protected array $returnFormat = [];

}
